# core imports
import rospy
import cv2
import numpy as np

# pub/sub imports
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import Image

# cv2 imports
import cv2, cv_bridge
from cv2 import startWindowThread, destroyAllWindows

PI = 3.1415926535897 # for rotation


objects_found = [False, False, False, False] # [yel, blu, red, grn]

objs = { # colour ranges for the 4 objects in HSV
    "yel" : [np.array([40,100,80]), np.array([80,255,255]),  True], # [min , max , found_bool]
    "blu" : [np.array([110,50,50]), np.array([130,255,255]), False], # for now only using yellow but the rest can stay here
    "red" : [np.array([245,50,50]), np.array([15,255,255]),  False],
    "grn" : [np.array([70,50,50]),  np.array([100,255,255]), False]
}


class dynamicMove():
    def __init__(self):
        cv2.startWindowThread()
        rospy.init_node('dynamicMove')
        # publisher init
        self.str_pub = rospy.Publisher('/msgs', String, queue_size=1)
        self.twist_pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=1)
        self.move = Twist()

        # subscriber init
        self.laser_sub = rospy.Subscriber('/scan', LaserScan, self.laser_callback)
        self.image_sub = rospy.Subscriber('/camera/rgb/image_raw', Image, self.img_callback)
    
        # other init
        self.bridge = cv_bridge.CvBridge()#
        self.laser_Array = [0]
        self.dist_infront = 999 # rogue (bigger)
        print("all nodes initialisesd")

    def laser_callback(self, laser): # store current laser values to be used elsewhere
        self.laser_Array = laser.ranges
        middle = len(laser.ranges)/2
        self.dist_infront = laser.ranges[middle]
        if self.dist_infront < 1:
            self.moveTurtle(0,0) # STOP

    def img_callback(self, msg): # called when new image data comes into program
        #cv2.namedWindow("Image window", 1)
        min_laser_dist = np.nanmin(self.laser_Array)

        if min_laser_dist < 0.5 :
            print("there is an object in the way")
            self.move.linear.x = 0
            self.hitObject()
        #print("image_callback")
        #print(msg)
        try:
            cv_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
            #print("image_created")
        except CvBridgeError, e:
            print e
        
        hsv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

        #hsv_grn = cv2.inRange(hsv_img, np.array(objs['grn'][0]), np.array(objs['grn'][1]))
        objs_in_sight = [False,False,False,False]
        if objs['yel'][2] == False: # yellow not found
            hsv_yel_thresh = cv2.inRange(hsv_img, np.array((20,100,80)), np.array((60,255,255)))
            y_moments = cv2.moments(hsv_yel_thresh)
            #cv2.imshow("Image window",hsv_yel_thresh)
            #cv2.waitKey(1)
            if y_moments['m00'] > 5000: # if moment is close to the camera
                objs['yel'][2] = self.move_to_object(hsv_img, hsv_yel_thresh)# sets to be true is object is found
                print("finding yellow", objs['yel'][2])

        elif objs['blu'][2] == False: # yellow not found
            hsv_blu_thresh = cv2.inRange(hsv_img, np.array((110,50,50)), np.array((130,255,255)))
            b_moments = cv2.moments(hsv_blu_thresh)
            #cv2.imshow("Image window",hsv_blu_thresh)
            #cv2.waitKey(1)
            if b_moments['m00'] > 5000:
                objs['blu'][2] = self.move_to_object(hsv_img, hsv_blu_thresh)
                print("finding blue")

        elif objs['red'][2] == False: # yellow not found
            hsv_red_thresh = cv2.inRange(hsv_img, np.array((0, 200, 30)), np.array((5, 255, 150)))
            r_moments = cv2.moments(hsv_red_thresh)
            #cv2.imshow("Image window2",hsv_red_thresh)
            #cv2.waitKey(1)
            if r_moments['m00'] > 5000:
                #objs['red'][2] = self.move_to_object(hsv_img, hsv_red_thresh)
                print("finding red")

        elif objs['grn'][2] == False: # yellow not found
            hsv_grn_thresh = cv2.inRange(hsv_img, np.array((70,50,50)), np.array((100,255,255)))
            g_moments = cv2.moments(hsv_grn_thresh) # see if there are any objects in this range
            #cv2.imshow("Image window",hsv_yel_thresh)
            #cv2.waitKey(1)
            if g_moments['m00'] > 5000:
               # objs['grn'][2] = self.move_to_object(hsv_img, hsv_grn_thresh)
                print("finding green")

        # y_moments['m00'] will return the number of pixels in this moment(img_object)
 

    def move_to_object(self, cv_img, thresh):
        print("moving to object called")
        im_height, im_width, im_depth = cv_img.shape
        camera_height_top = im_height*0.75
        camera_height_low = im_height*0.5
        M = cv2.moments(thresh)
        rotate_angle = 100
        # check for the threshold object infront of the camera
        if thresh[camera_height_top:,:].any() and self.dist_infront < 1.1:
            #rospy.sleep(3)
            print("object found")
            return True

        if M['m00'] > 0: # baso if the mask has found something... 
            x_centre = int(M['m10']/M['m00']) # x centroid
            y_centre = int(M['m01']/M['m00']) # y centroid
            error = x_centre - im_width/2 # distance between moment(object) centroid and the centre of the image

            rotate_angle = -float(error) / 100 # point the robot in the right direction
            print(rotate_angle) # the degree of turning the robot has to take to point towards the object
            self.moveTurtle(0,rotate_angle) # so the object is straight infront of the object
            # x = input("hold")
            if rotate_angle < 0.1 and rotate_angle > -0.1 :
                while self.dist_infront > 0.75 or rotate_angle < 0.1: # until the object is right infront of the robot
                    #print("moving forward")
                    self.moveTurtle(0.25,0) # inch forward

        return False


    def hitObject(self):
        print("robot encountered obstical")
        for i in range(0,10):
            self.move.linear.x = -0.5
            self.twist_pub.publish(self.move)
        self.move.angular.z = PI
        self.twist_pub.publish(self.move)


       

    def moveTurtle(self, linear, angular):
        min_laser_dist = np.nanmin(self.laser_Array)
        if min_laser_dist < 0.5 :
            print("there is an object in the way")
            self.move.linear.x = 0
            self.hitObject()
        else:
            self.move.linear.x = linear
        self.move.angular.z = angular
        self.twist_pub.publish(self.move)
        print("twist// Angular: {0} , Linear: {1}".format(self.move.angular.z, self.move.linear.x))
        



    def find_obj(self, image_data): # called from image callback when object not found
        try:
            cv_image = self.bridge.imgmsg_to_cv2(image_data, "bgr8") # convert image to cv2(able to the use data)
        except CvBridgeError, e:
            print e

        hsv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV) # convert image from bgr to hsv
        self.moveTurtle(0.2,0) # lin = 0.2, ang = 0





d = dynamicMove() # create an instance of the class (instantiation)
#d.laser_in() # call laser_in over the instance of the class
rospy.spin() # stops the program from shutting down
cv2.destroyAllWindows()