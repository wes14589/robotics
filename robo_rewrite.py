# core imports
import rospy
import cv2
import numpy as np

# pub/sub imports
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import Image

# cv2 imports
import cv2, cv_bridge
from cv2 import startWindowThread, destroyAllWindows

PI = 3.1415926535897 # for rotation

class dynamicMove():
    def __init__(self): # initialisation of all node and variables
        cv2.startWindowThread()
        rospy.init_node('dynamicMove')
        # publisher init
        self.str_pub = rospy.Publisher('/msgs', String, queue_size=1)
        self.twist_pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=1)
        self.move = Twist()

        # subscriber init
        self.laser_sub = rospy.Subscriber('/scan', LaserScan, self.laser_callback)
        self.image_sub = rospy.Subscriber('/camera/rgb/image_raw', Image, self.image_callback)
    
        # other init
        self.bridge = cv_bridge.CvBridge()#
        self.laser_Array = [0]
        self.dist_infront = 999 # rogue (bigger)
        self.min_laser_dist = 999
        self.found = [False,False,True,False] # to remember which objects have been found when 
        self.lost = False
        self.robot_state = 0 # 0 = No Object, 1 = Found Object, 2 = Object Search
        self.s0_angle = 13*PI
        self.obstruction = False # mark as true when laser detects object in path, stop robot and recalculate path
        #self.found = [False,False,False,False] # yellow, blue, red, green

        print("all nodes initialisesd")

    def laser_callback(self, laser): # store current laser values to be used elsewhere in the program
        # ONLY CHANGES THE VARIABLES - DOESN'T STOP ROBOT IF CLOSE
        self.laser_Array = np.array(laser.ranges)
        middle = len(laser.ranges)/2
        self.dist_infront = laser.ranges[middle]
        self.min_laser_dist = np.nanmin(self.laser_Array)
        if self.min_laser_dist <1:
            self.obstruction = True
        print(self.laser_Array.shape)

        #print(self.min_laser_dist)



    def image_callback(self, img):
        if self.robot_state == 0:
            self.state_0(img) # No Object
        elif self.robot_state == 1:
            self.state_1(img) # Found Object
        elif self.robot_state == 2:
            self.state_2(img) # Object Search


    def only_rotate(self, angular):
        self.move.angular.z = angular
        self.twist_pub.publish(self.move)

        return 0 

    def move_robot(self,linear, angular):
        print("moving {0}, {1}".format(linear,angular))



    def state_0(self, img): # No Object
# aim of this state is to rotate the robot and find an already unidentified object
# once object is found state will change to state 1
# if no object is found in the line of sight then the robot will change into state 2

        if self.s0_angle < 0 :
            self.state = 2
            return None
        object_angle = self.image_processing(img)
        #print("robot state check 1 =", self.robot_state)
        print("returned angle = ", object_angle)
        if object_angle == None and self.s0_angle >0: # no object found so rotate
            self.turtle_move(0,PI/4) # only rotation, so no object avoidance needed as should not move
            self.s0_angle -= PI/16
        else: # an object has been found, move towards it
            self.robot_state = 1 # Found object state
            print("state changed")
        print("robot state and s0_angle check 2 ={0}, {1}".format( self.robot_state, self.s0_angle))






    def state_1(self, img): # Found Object
        print("robot in state 1 - move to find the object")#
        if not self.obstruction :
            print("nohting in the way keep moving...")
        else:
            self.avoid_obsticle(img)
            print("recalculate path here...")

        if self.min_laser_dist <1:
            self.obstruction = True
        # when reset back to s0...
        self.s0_angle = 13*PI
        return 0

    def state_2(self, img): # Object search
# PURPOSE OF THIS STATE IS TO MOVE ACROSS THE MAP LOOKING FOR ANOTHER OBJECT
# REQUIRES SOME FORM OF MAPPING TO KNOW WHERE THE ROBOT IS IN RELATION TO WALLS(ETC)
# NEED TO SET WAYPOINTS FOR THE ROBOT TO TRAVSERE BETWEEN AND CHECK FOR OBJECTS FROM
#   THOSE LOCATIONS
        print("robot in state 2 - traverse the map.")
        return 0

    def avoid_obsticle(self, img):
# - CHANGE THE PATH OF THE ROBOT AWAY FROM THE OBSTICAL SO THAT IT CAN REACH THE ROBOT 
# WITHOUT HITTING ANYTHING IN ITS PATH
# - FIRST FIND WHICH SIDE THE OBSTICAL IS COMPARED TO THE ROBOT
# - THEN TURN AWAY FROM THAT SIDE AND MOVE FORWARD
        object_index = np.where(self.laser_Array == self.min_laser_dist)
        if object_index < len(self.laser_Array)/2:
            # OBSTRUCTION ON THE LEFT OF THE ROBOT
            print("object on left - so turn to the right")
        else:
            # OBSTRUCTION ON RIGHT OF THE ROBOT
            print("object on the right so turn to the left")
        return 0 

    def turtle_move(self, linear, angular):
        self.move.linear.x = linear
        self.move.angular.z = angular
        self.twist_pub.publish(self.move)
        return 0 

    def image_processing(self, image_raw): # look for an object, return angle or None
        print("proessing image, looking for objects")
        angular = None

        # CREATE THE THRESHOLDS

        try:
            cv_image = self.bridge.imgmsg_to_cv2(image_raw, "bgr8")
            #print("image_created")
        except  CvBridgeError, e:
            print e
        
        height,width,depth = cv_image.shape
        hsv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
        hsv_yel_thresh = cv2.inRange(hsv_img, np.array((20,100,80)), np.array((60,255,255)))
        y_moments = cv2.moments(hsv_yel_thresh)
        hsv_blu_thresh = cv2.inRange(hsv_img, np.array((110,50,50)), np.array((130,255,255)))
        b_moments = cv2.moments(hsv_blu_thresh)
        hsv_red_thresh = cv2.inRange(hsv_img, np.array((0, 200, 30)), np.array((5, 255, 150)))
        r_moments = cv2.moments(hsv_red_thresh)
        hsv_grn_thresh = cv2.inRange(hsv_img, np.array((70,50,50)), np.array((100,255,255)))
        g_moments = cv2.moments(hsv_grn_thresh) 

        if y_moments['m00'] > 5000 and not self.found[0]: # if yellow is in view and not already found
            # find the angle
            print("find yellow")
            angular = self.find_angle(y_moments, width)
        elif b_moments['m00'] > 5000 and not self.found[1]: # if blue is in view and not already found
            # find the angle
            print("find blue")
            angular = self.find_angle(b_moments, width)
        elif r_moments['m00'] > 5000 and not self.found[2]: # if red is in view and not already found
            # find the angle
            print("find red")
            angular = self.find_angle(r_moments, width)
        elif g_moments['m00'] > 5000 and not self.found[3]: # if green is in view and not already found
            # find the angle
            print("find green")
            angular = self.find_angle(g_moments, width)
        #print(angular)
        return angular


    def find_angle(self, moment, width): # find the angle of rotation
        print("finding angle")
        x_centre = int(moment['m10']/moment['m00'])
        y_centre = int(moment['m01']/moment['m00'])
        error = x_centre - width/2
        angle = -float(error) / 100

        return angle



if __name__ == "__main__":
    d = dynamicMove() # create an instance of the class (instantiation)
    #d.laser_in() # call laser_in over the instance of the class
    rospy.spin() # stops the program from shutting down
    cv2.destroyAllWindows()