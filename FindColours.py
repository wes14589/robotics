# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 21:39:35 2019

@author: student
"""

import rospy
import cv2
import numpy as np
from cv2 import startWindowThread, destroyAllWindows
from sensor_msgs.msg import Image # used to get the image from the robot into the program (subscriber)
from cv_bridge import CvBridge # this is the library which allows you to convert rospy images to cv2 images to allow manipulation
from std_msgs.msg import String # used to send messages from the program to the console (publisher)


class findColours:
    def __init__(self):
        self.bridge = CvBridge() # initialise cv_bridge
        self.image_sub = rospy.Subscriber("/camera/rgb/image_raw", Image, self.callback) # enables program to get images from the robot
        self.pub=rospy.Publisher('/result_topic', String) # enables program to publish strings to the console when running
        
    def callback(self, msg):
        cv2.namedWindow("window1", 1) # open a new window (to show robot image)
        cv2.namedWindow("window2", 1)
        image = self.bridge.imgmsg_to_cv2(msg) #  takes the ros image and converts to cv2 image (able to use)
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV) # takes cv2 image (BGR) converts to HSV (Hue, Saturation, Value/Intensity)
        low_yellow = np.array([40,100,80]) # low boundry for yellow
        high_yellow = np.array([80,255,255]) # upper boundry for yellow
        low_blue = np.array([110,50,50])
        high_blue = np.array([130,255,255])
        low_green = np.array([70,50,50])
        high_green = np.array([100,255,255])
        low_red = np.array([245, 50, 50])
        high_red = np.array([15,255,255])
        
        ymask = cv2.inRange(hsv, low_yellow, high_yellow) # threshold the image to only get colours in range
        bmask = cv2.inRange(hsv, low_blue, high_blue)
        gmask = cv2.inRange(hsv, low_green, high_green)
        rmask = cv2.inRange(hsv, low_red, high_red)

        
        
        cv2.bitwise_and(image, image, ymask=ymask) # bitwise AND to only show colours in that range
        cv2.bitwise_and(image, image, bmask=bmask)
        cv2.bitwise_and(image, image, gmask=gmask)
        cv2.bitwise_and(image, image, rmask=rmask)
        
        cv2.imshow("window1 - yellow mask", ymask) # show the masked image in the created window
        cv2.imshow("window2 - blue mask", bmask)
        cv2.imshow("window3 - green mask", gmask)
        cv2.imshow("window4 - red mask", rmask)        
        
        cv2.imshow("window0 - raw image",image)
        cv2.waitKey(1)
        
        self.pub.publish("Find me the yellow!") # publish this string to the console when called
        
startWindowThread()
rospy.init_node('findColours')
fc = findColours()
rospy.spin()

destroyAllWindows()
        