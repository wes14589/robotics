import rospy
import numpy as np
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist

class slow_down:
	def __init__(self):
	    rospy.init_node('slow_down')
	    self.subscriber = rospy.Subscriber(
	        '/scan', LaserScan, callback=self.cb) # when program gets data from LaserScan subscriber run cb() function
	    self.p = rospy.Publisher(
	        '/mobile_base/commands/velocity', Twist, queue_size=1)

	def cb(self, incoming_data): # incoming_data is the laserScan object 
	    # print len(incoming_data.ranges)
	    min_dist = 1.5 # 1.5m
	   
	    min_angle_check =  90//360 * incoming_data.ranges.size # finds array index to start loop
	    max_angle_check = 270/360 * incoming_data.ranges.size #   ' '  to finish loop - (angles in front of bot)

	    for i in range(min_angle_check, max_angle_check): # should be to 720
	    	print(i, " - ", incoming_data.ranges[i]) 
	    	if incoming_data.ranges[i] <= min_dist:
	    		t = Twist()
	    		t.linear.x = t.linear.x * 0.5 # incremental slowing down of linear velocity
	    		self.p.publish(t)




rec = slow_down()
rospy.spin()